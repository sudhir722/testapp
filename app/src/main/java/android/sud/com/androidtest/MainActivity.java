package android.sud.com.androidtest;

import android.net.Uri;
import android.os.Bundle;
import android.sud.com.androidtest.listeners.OnListViewClick;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import static android.sud.com.androidtest.utils.LocalConstants.KEY_DESC;
import static android.sud.com.androidtest.utils.LocalConstants.KEY_IMG_URL;
import static android.sud.com.androidtest.utils.LocalConstants.KEY_TITLE;

public class MainActivity extends AppCompatActivity implements FullViewFragment.OnFragmentInteractionListener,OnListViewClick {
    Toolbar toolbar;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivityFragment myFrag = (MainActivityFragment)
                        getSupportFragmentManager().findFragmentById(R.id.fragment);
                if (myFrag != null) {
                    // If article frag is available, we're in two-pane layout...
                    // Call a method in the ArticleFragment to update its content
                    myFrag.refreshList();
                }
            }
        });

    }

    public void openExtras(String title, String description,String imgHref){
        Fragment yourFragment = new FullViewFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_TITLE,title);
        bundle.putString(KEY_DESC,description);
        bundle.putString(KEY_IMG_URL,imgHref);
        yourFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment, yourFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onListViewClick(String title, String desc, String imghref) {
        fab.setVisibility(View.GONE);
        openExtras(title,desc,imghref);
    }

    @Override
    public void onTitleChange(String title) {
        if(toolbar!=null) {
            toolbar.setTitle(title);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        fab.setVisibility(View.VISIBLE);
        return super.onKeyDown(keyCode, event);

    }
}
