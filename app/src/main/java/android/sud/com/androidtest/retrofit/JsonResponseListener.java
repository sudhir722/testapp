package android.sud.com.androidtest.retrofit;

import org.json.JSONObject;

public interface JsonResponseListener {
    public void onSuccess(JSONObject responseObject);
    public void onFailuar(String error);
}
