package android.sud.com.androidtest;

import android.content.Context;
import android.sud.com.androidtest.adaptar.CustomListAdaptar;
import android.sud.com.androidtest.listeners.OnListViewClick;
import android.sud.com.androidtest.retrofit.BaseFragment;
import android.sud.com.androidtest.retrofit.IApiMethods;
import android.sud.com.androidtest.retrofit.JsonResponseListener;
import android.sud.com.androidtest.retrofit.RetrofitServices;
import android.sud.com.androidtest.retrofit.models.JsonResponse;
import android.sud.com.androidtest.retrofit.models.SubModel;
import android.sud.com.androidtest.utils.LocalPref;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.JsonObject;
import com.yalantis.phoenix.PullToRefreshView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends BaseFragment implements JsonResponseListener{

    ListView listView;
    CustomListAdaptar adapter = null;
    List<SubModel> modelList = new ArrayList<>();
    private PullToRefreshView mPullToRefreshView;
    public static final int REFRESH_DELAY = 2000;
    OnListViewClick mCallback;

    public MainActivityFragment() {
    }

    public void refreshList(){
        loadJsonData(this);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_main, container, false);
        mPullToRefreshView = (PullToRefreshView) view.findViewById(R.id.pull_to_refresh);
        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mPullToRefreshView.setRefreshing(false);
                        loadJsonData(MainActivityFragment.this);
                    }
                }, REFRESH_DELAY);
            }
        });
        listView = (ListView) view.findViewById(R.id.list_contenar);
        loadOldDataList();

        adapter = new CustomListAdaptar(getActivity(), 0, modelList,mCallback);
        listView.setAdapter(adapter);
        loadJsonData(this);

        return view;
    }

    private void loadOldDataList() {
        modelList.clear();
        try {
            JSONObject jsonObject = new JSONObject(LocalPref.getJList(getActivity()));
            JSONArray jsonArray = jsonObject.getJSONArray("rows");
            mCallback.onTitleChange(jsonObject.getString("title"));
            for (int index=0;index < jsonArray.length();index++){
                JSONObject obj = jsonArray.getJSONObject(index);
                modelList.add(new SubModel(obj.getString("title"),obj.getString("description"),obj.getString("imageHref")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (OnListViewClick) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement HeadlineListener");
        }
    }

    @Override
    public void onSuccess(JSONObject responseObject) {
        modelList.clear();
        try {
            JSONArray jsonArray = responseObject.getJSONArray("rows");
            mCallback.onTitleChange(responseObject.getString("title"));
            for (int index=0;index < jsonArray.length();index++){
                JSONObject obj = jsonArray.getJSONObject(index);
                modelList.add(new SubModel(obj.getString("title"),obj.getString("description"),obj.getString("imageHref")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onFailuar(String error) {

    }
}
