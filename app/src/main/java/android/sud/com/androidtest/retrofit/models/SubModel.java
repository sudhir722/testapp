package android.sud.com.androidtest.retrofit.models;

import android.text.TextUtils;

public class SubModel {
    String title;
    String description;
    String imageHref;

    public SubModel(String title, String description, String imageHref) {
        this.title = title;
        this.description = description;
        this.imageHref = imageHref;
    }

    public SubModel() {
    }

    public String getTitle() {
        return title;

    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        if(TextUtils.isEmpty(description)){
         return "No Description";
        }else {
            return description;
        }
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageHref() {
        return imageHref;
    }

    public void setImageHref(String imageHref) {
        this.imageHref = imageHref;
    }
}
