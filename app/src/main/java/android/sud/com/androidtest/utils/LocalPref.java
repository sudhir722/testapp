package android.sud.com.androidtest.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class LocalPref {
    public static final String KEY_PREF_VALUE = "pr_";
    public static final String KEY_JLIST = "jlist";
    public static final String KEY_BLANK = "";

    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(KEY_PREF_VALUE, Context.MODE_PRIVATE);
    }

    public static void putString(Context context, String key, String value) {
        getPreferences(context).edit().putString(key, value).commit();
    }

    public static String getString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    public static String getJList(Context context) {
        return getPreferences(context).getString(KEY_JLIST,KEY_BLANK);
    }

    public static void setKeyJlist(Context context, String jsonList) {
        getPreferences(context).edit().putString(KEY_JLIST, jsonList).commit();
    }


}
