package android.sud.com.androidtest.retrofit;


import android.sud.com.androidtest.retrofit.models.JsonResponse;
import android.sud.com.androidtest.utils.LocalConstants;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by sudhir722 on 18-07-2018.
 */
public interface IApiMethods {

    @Headers({
            "Accept: application/json"
    })


    @GET(LocalConstants.API_JSONLIST)
    Call<JsonObject>  apiJList();

    @GET(LocalConstants.API_JSONLIST)
    Call<JsonResponse>  apiJListModel();
}
