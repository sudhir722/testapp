package android.sud.com.androidtest.retrofit.models;

import java.util.List;

public class JsonResponse {

    String title;
    List<SubModel> subModels;

    public JsonResponse(String title, List<SubModel> subModels) {
        this.title = title;
        this.subModels = subModels;
    }

    public JsonResponse() {

    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<SubModel> getSubModels() {
        return subModels;
    }

    public void setSubModels(List<SubModel> subModels) {
        this.subModels = subModels;
    }
}
