package android.sud.com.androidtest.retrofit;

import android.content.Context;
import android.sud.com.androidtest.BuildConfig;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitServices implements Serializable {

    protected final static String BASE_URL = "https://dl.dropboxusercontent.com/";
    protected final static int REQUEST_TIMEOUT = 5;

    protected static RetrofitServices mObject;

    private RetrofitServices(){

    }
    public static RetrofitServices getInstance(){
        if(mObject==null){
            mObject = new RetrofitServices();
        }
        return mObject;
    }

    public static Retrofit getAdaptor(final Context context) {

        final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.connectTimeout(REQUEST_TIMEOUT, TimeUnit.MINUTES)
                    .writeTimeout(REQUEST_TIMEOUT, TimeUnit.MINUTES)
                    .readTimeout(REQUEST_TIMEOUT, TimeUnit.MINUTES);
            httpClient.addInterceptor(logging);
        } else {
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    Request request = original.newBuilder()
                            .header("User-Agent", System.getProperty("http.agent"))
                            .header("Accept", "application/json")
                            .method(original.method(), original.body())
                            .build();
                    return chain.proceed(request);
                }
            });
        }
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();
        return retrofit;
    }

}
