package android.sud.com.androidtest.listeners;

public interface OnListViewClick {
    public void onListViewClick(String title,String desc,String imghref);
    public void onTitleChange(String title);
}
