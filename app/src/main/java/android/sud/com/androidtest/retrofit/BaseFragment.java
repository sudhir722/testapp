package android.sud.com.androidtest.retrofit;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.sud.com.androidtest.utils.LocalPref;
import android.sud.com.androidtest.utils.Utility;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseFragment extends Fragment {

    ProgressDialog mProgressBar = null;
    private void initDialog(){
        if(!isVisible()) {
            mProgressBar = new ProgressDialog(getActivity());
            mProgressBar.setCancelable(true);//you can cancel it by pressing back button
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDialog();
    }
    public void showProgressDialog(String message){
        if(isVisible()) {
            if (mProgressBar == null) {
                initDialog();
            }
            mProgressBar.setMessage(message);
            mProgressBar.show();
        }
    }

    public void hideDialog(){
        if(isVisible()) {
            if (mProgressBar != null && mProgressBar.isShowing()) {
                mProgressBar.dismiss();
            }
        }
    }
    public void showErrorAlert(String title,String message){
        if(isVisible()) {
            if (builder == null) {
                initAlertDialog();
            }
            builder.setTitle(title).setMessage(message).show();
        }
    }
    AlertDialog.Builder builder;
    private void initAlertDialog(){
        if(isVisible()) {
            builder = new AlertDialog.Builder(getActivity());
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            // Create the AlertDialog object and return it
            builder.create();
        }
    }

    public void loadJsonData(final JsonResponseListener mListener){
        if(!Utility.isConnected(getActivity())){
            showErrorAlert("Internet Unavaliable","Please check your internet connectivity...");
            return;
        }
        showProgressDialog("Loading...");
        IApiMethods api = RetrofitServices.getAdaptor(getActivity()).create(IApiMethods.class);
        api.apiJList().enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JSONObject jListData = null;
                try {
                    jListData = new JSONObject(response.body().toString());
                    if(jListData!=null && getActivity()!=null){
                        LocalPref.setKeyJlist(getActivity(),jListData.toString());
                        mListener.onSuccess(jListData);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(getContext()!=null) {
                    Toast.makeText(getContext(), "List Load successfully", Toast.LENGTH_LONG).show();
                }
                hideDialog();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                hideDialog();
                Log.d("JSONS","error");
            }
        });
    }
}
