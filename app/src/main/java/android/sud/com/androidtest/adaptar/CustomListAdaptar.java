package android.sud.com.androidtest.adaptar;

import android.content.Context;
import android.sud.com.androidtest.R;
import android.sud.com.androidtest.listeners.OnListViewClick;
import android.sud.com.androidtest.retrofit.models.SubModel;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;

public class CustomListAdaptar extends ArrayAdapter<SubModel> {
    Context mContext = null;
    static List<SubModel> mList;
    OnListViewClick mListener;

    public CustomListAdaptar(@NonNull Context context, int resource, @NonNull List<SubModel> objects,OnListViewClick mCallback) {
        super(context, resource, objects);
        mContext = context;
        mList = objects;
        mListener = mCallback;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.list_row, null);
        ButterKnife.bind(this,convertView);
        ImageView subimage  = (ImageView) convertView.findViewById(R.id.img_subtitle);
        TextView subText  = (TextView) convertView.findViewById(R.id.txt_subtitle);
        TextView title  = (TextView) convertView.findViewById(R.id.txt_title);

        final SubModel model = mList.get(position);
        Picasso.get().load(model.getImageHref()).into(subimage);
        subText.setText(model.getDescription());
        title.setText(model.getTitle());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onListViewClick(model.getTitle(),model.getDescription(),model.getImageHref());
            }
        });
        return convertView;
    }
}
